﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Microsoft.EntityFrameworkCore;
using Model.Entities;
using Model.Interfaces;
using Model.Models;
using Security.Helper;
using Service.Utils;

namespace Service
{
    public class PersonService : SecurityService, IPersonService
    {
        private readonly ApplicationDbContext _context;

        public PersonService(ApplicationDbContext db)
        {
            _context = db;
        }

        public IEnumerable<PersonModel> GetAll()
        {
            return Query();
        }

        public PersonModel Get(int id)
        {
            return Query().FirstOrDefault(x => x.Id == id);
        }

        public PersonModel Save(PersonModel model)
        {
            var entity = model.GetModel(new Person());

            _context.Entry(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Modified;
            _context.SaveChanges();

            return Get(entity.Id);
        }

        public bool Delete(int id)
        {
            _context.Remove(_context.Persons.Find(id));

            return _context.SaveChanges() > 0;
        }

        public IQueryable<PersonModel> Query()
        {
            return from a in _context.Persons
                select new PersonModel
                {
                    Id = a.Id,
                    BirthDate = a.BirthDate,
                    Name = a.Name,
                    Age = DateTime.Today.Year - a.BirthDate.Year
                };
        }
    }
}
