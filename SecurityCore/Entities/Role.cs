//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using Newtonsoft.Json;

//namespace SecuritySGD.Entities
//{
//    [Table("SECRole")]
//    public class Role
//    {
//        public Role()
//        {
//            UserRole = new HashSet<UserRole>();
//        }

//        [Key]
//        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
//        public int Id { get; set; }

//        [Required]
//        [StringLength(50)]
//        public string Name { get; set; }

//        [StringLength(20)]
//        public string CompaniaId { get; set; }

//        [JsonIgnore]
//        public IEnumerable<UserRole> UserRole { get; set; }
//    }
//}