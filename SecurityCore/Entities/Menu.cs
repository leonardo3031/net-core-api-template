using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Security.Helper;

namespace Security.Entities
{
    [Table("SECMenu")]
    public class Menu
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Data { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        public int ParentId { get; set; }

        public int? AccessFormId { get; set; }

        [JsonIgnore]
        public AccessForm AccessForm { get; set; }
    }

    [NotMapped]
    public class MenuModel : Menu
    {
        public Menu ToMenu()
        {
            return this.ToClass(new Menu());
        }
    }
}