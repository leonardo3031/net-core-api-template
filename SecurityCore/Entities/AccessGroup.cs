//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using Newtonsoft.Json;

//namespace SecuritySGD.Entities
//{
//    [Table("SECAccessGroup")]
//    public class AccessGroup
//    {
//        public AccessGroup()
//        {
//            AccessForm = new HashSet<AccessForm>();
//        }

//        [Key]
//        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
//        public int Id { get; set; }

//        public string Name { get; set; }

//        [StringLength(20)]
//        public string CompaniaId { get; set; }

//        [JsonIgnore]
//        public IEnumerable<AccessForm> AccessForm { get; set; }
//    }
//}