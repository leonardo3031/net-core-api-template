using Microsoft.EntityFrameworkCore.Design;
using Security.Helper;

namespace Security.DataLayer
{
    public class SecurityDbContextFactory : IDesignTimeDbContextFactory<SecurityDbContext>
    {
        public SecurityDbContext CreateDbContext(string[] args)
        {
            return App.GetContext<SecurityDbContext>();
        }
    }
}