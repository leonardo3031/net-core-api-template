﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;
using Security.DataLayer;
using Security.Entities;

namespace Data
{
    public class DbInitializer
    {
        public static void Seed(ApplicationDbContext context, SecurityDbContext securityContext)
        {
            if(securityContext.AccessForm.Any())
                return;

            securityContext.AccessForm.AddRange(new List<AccessForm>()
            {
                new AccessForm(){ FormName = "Person", Description = null }
            });
            
            securityContext.UserAccess.AddRange(new List<UserAccess>()
            {
                new UserAccess{ Action = 2, AccessFormId = 1, UserId = 1 },
                new UserAccess{ Action = 4, AccessFormId = 1, UserId = 1 },
                new UserAccess{ Action = 8, AccessFormId = 1, UserId = 1 },
                new UserAccess{ Action = 16, AccessFormId = 1, UserId = 1 }
            });

            securityContext.SaveChanges();
        }
    }
}
