﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Model.Models;

namespace Model.Interfaces
{
    public interface IPersonService
    {
        IEnumerable<PersonModel> GetAll();
        PersonModel Get(int id);
        PersonModel Save(PersonModel model);
        bool Delete(int id);
        IQueryable<PersonModel> Query();
    }
}