﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.Models
{
    public class PersonModel : Person
    {
        public int Age { get; set; }
    }
}
