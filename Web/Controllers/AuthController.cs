﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Security.Controllers;
using Security.DataLayer;
using Security.Entities;
using Security.Services;
using Service.Utils;

namespace Web.Controllers
{
    public class AuthController : SecurityController
    {
        private readonly IAuthService _authService;
        private readonly ITokenStoreService _tokenStoreService;
        private readonly SecurityDbContext _securityContext;

        public AuthController(IAuthService authService, ITokenStoreService tokenStoreService, SecurityDbContext securityContext) : base(authService, tokenStoreService, securityContext)
        {
            _authService = authService;
            _tokenStoreService = tokenStoreService;
            _securityContext = securityContext;
        }

        public override void AfterLogin(UserModel user)
        {
            user.Data = new GlobalConfig
            {
                UserId = user.Id
            };
        }
    }
}