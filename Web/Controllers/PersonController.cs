﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.Interfaces;
using Model.Models;
using Security.Utils;
using Service;
using Web.Utils;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class PersonController : SecurityController<PersonService>
    {
        private readonly PersonService _personService;

        public PersonController(PersonService personService) : base(personService, "Person")
        {
            _personService = personService;
        }
        
        [HttpGet]
        [CustomAuthorize(CrudAction.Read)]
        public IDataResult GetAll()
        {
            try
            {
                return new CustomResult(_personService.GetAll());
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public IDataResult Get(int id)
        {
            try
            {
                return new CustomResult(_personService.Get(id));
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }

        [HttpPost]
        public IDataResult Add([FromBody] PersonModel model)
        {
            try
            {
                return new CustomResult(_personService.Save(model));
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }

        [HttpPut]
        public IDataResult Edit([FromBody] PersonModel model)
        {
            try
            {
                return new CustomResult(_personService.Save(model));
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public IDataResult Delete([FromRoute] int id)
        {
            try
            {
                return new CustomResult(_personService.Delete(id));
            }
            catch (Exception e)
            {
                throw new ExceptionResult(e);
            }
        }
    }
}