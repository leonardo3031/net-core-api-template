﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Utils
{
    public class DataResult
    {
        public object DataSource { get; set; }
        public string Message { get; set; }
        public bool Commit { get; set; }
        //public int TotalItems { get; set; }
    }

    public interface IDataResult
    {
        DataResult Data { get; set; }
    }

    public class CustomResult : IDataResult
    {
        public DataResult Data { get; set; }

        public CustomResult(object dataSource, string message, bool commit)
        {
            this.Data = new DataResult
            {
                DataSource = dataSource,
                Message = message,
                Commit = commit
            };
        }

        public CustomResult(object dataSource, string message) : this(dataSource, message, true)
        {
        }

        public CustomResult(object dataSource) : this(dataSource, "", true)
        {
        }
    }

    public class ExceptionResult : Exception, IDataResult
    {
        public DataResult Data { get; set; }

        public ExceptionResult(Exception e)
        {
            this.Data = new DataResult
            {
                DataSource = null,
                Commit = false,
                Message = e.Message
            };
        }
    }
}
